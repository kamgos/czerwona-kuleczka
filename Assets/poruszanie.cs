using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using TMPro;
using JetBrains.Annotations;

public class Test : MonoBehaviour
{
    public float Speed;
    Vector3 movement;
    private Rigidbody rb;
    private int amountCoins;
    public TextMeshProUGUI coinTxt;


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P)) 
        {
            if (GameIsPaused)
            { Resume(); }
            else 
            {
                Pause();
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        float carSpeed = 7.0f; // Zmniejszona pr�dko�� samochodu
        float moveHorizontal = Input.GetAxisRaw("Horizontal"); // Pobierz ruch w lewo i w prawo (A, D)
        float moveVertical = Input.GetAxisRaw("Vertical"); // Pobierz ruch do przodu i do ty�u (W, S)       

        // Okre�lenie ruchu samochodu
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        transform.Translate(movement * carSpeed * Time.deltaTime); // Ruch samochodu

    }
    public void Resume()
    {
        pausemenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        Cursor.visible = false;
        desiredMode = CursorLockMode.Confined;

    }
    void Pause()
    {
        pausemenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        Cursor.visible = true;
        desiredMode = CursorLockMode.None;
        {
            Cursor.lockState = desiredMode;
        }

    }
    public void Hat1()
    {
        if (amountCoins >= 2)
        {
            hat1.gameObject.SetActive(true);
            amountCoins = amountCoins - 2;
            coinTxt.text = amountCoins.ToString();
        }
    }
    public void Hat2()
    {
        if (amountCoins >= 4)
        {
            hat2.gameObject.SetActive(true);
            amountCoins = amountCoins - 4;
            coinTxt.text = amountCoins.ToString();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
            amountCoins = amountCoins + 1;
            coinTxt.text = amountCoins.ToString();
        }
        if (other.gameObject.CompareTag("Size"))
        {
            transform.localScale += new Vector3(10f, 10f, 10f);

        }
        if (other.gameObject.CompareTag("DoorArea") && amountCoins >= 2)
        {
            other.GetComponent<Animator>().SetTrigger("Door");
        }
    }
    public GameObject hat1;
    public GameObject hat2;
    public GameObject pausemenuUI;

    CursorLockMode desiredMode;
    public static bool GameIsPaused = false;

}
